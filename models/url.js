'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UrlSchema  = Schema({
    link:String,
    code:String
});

module.exports  = mongoose.model('Url',UrlSchema);