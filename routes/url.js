'use strict'


var express = require('express');
var UrlController = require('../controllers/url');

var multipart = require('connect-multiparty');
var md_upload = multipart();


var api = express.Router();
api.post('/encode',UrlController.encode);
api.get('/:code?',UrlController.openUrl);
api.post('/file',md_upload,UrlController.readFile);

module.exports = api;

