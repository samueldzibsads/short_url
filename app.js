'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app =express();

//Load route
var url_routes = require('./routes/url');

//Middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


//Route
app.use('/',url_routes);


//Export configuration
module.exports = app;