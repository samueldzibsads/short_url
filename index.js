'use strict'

var mongoose = require('mongoose');
var app  = require('./app');
var port =3800;

//config connection db
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/shortUrl',{ useNewUrlParser: true } ).then(()=>{
    console.log('Conexion exitosa');
    //Server create
    app.listen(port,()=>{
        console.log('Run server');
    });

}).catch(err=>console.log(err));

