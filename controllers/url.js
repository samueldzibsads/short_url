'use strict'

var Url  = require('../models/url');


function encode(req,res) {
    var url_server = req.get('host');
    var params = req.body;
    var link = params.url;
    var url = new Url();
    if(link){
        url.link = link;

        Url.findOne({link: url.link}).exec((err,success)=>{

            if(err){
                return res.status(500).send({
                    message:'Error verificando url'
                });
            }

            if(success){
                return res.status(200).send({url:(url_server+'/'+success.code)});
            }else{
                url.code  = generateUrl();
                url.save((err,response)=>{
                    if(err) return res.status(500).send({
                        message:'Error al generar la url corta'
                    });
                    if(response){
                        res.status(200).send({url:(url_server+'/'+response.code)})
                    }else{
                        res.status(404).send({message:'Error, la url no fue generada correctamente'});
                    }
                });

            }
        });





    }else{
        res.status(200).send({
            message: 'Envia los campos necesarios'
        });
    }
}

function generateUrl(){
    const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    let string_length = 6;
    var randomstring = "";


        for (let i = 0; i < string_length; i++) {
            let rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;

}


function openUrl(req,res) {


    if(req.params.code){
        var code = req.params.code;
        var url = new Url();
        url.code  = code;
        Url.findOne({code: url.code}).exec((err,success)=>{
            if(err){
                return res.status(500).send({
                    message:'Error con el servidor'
                });
            }
            if(success){
                return res.status(200).send({url: success.link});
            }

        });

    }else{
        res.status(404).send({
            message: 'Url incompleta'
        });
    }



}

async function readFile(req,res){
    var url_server = req.get('host');
    if(req.files){
        var file_path = req.files.file.path;

        var fs = require('fs');
        var response = [];
        let archivo = fs.readFileSync(file_path, 'utf8');
        let lines = archivo.toString().split('\n');
       var finish_response = await for_file(lines,response,url_server);
        return res.status(200).send({
            'url':finish_response
        });
    }

}
async function for_file(lines,response,url_server){
    var i=0;
    var array = [];
    for(const item of lines){
        var url = new Url();
        url.link= item.toString().replace("\r","");
        array[i] = await insert_file(url,i,url_server).then((value)=>{
            return  value;
        });
        i++;
    }
    return array;
}

function insert_file(url, i,url_server){

    return new Promise(async function(resolve, reject){
        await Url.findOne({link: url.link}).exec((err,success)=>{

            if(err) return handleError(err);
            if(success){
                resolve(url_server+'/'+success.code);
            }
            else{
            insert_file_not_save(url,url_server).then((solve)=>{
                    resolve(solve);
                });

            }


        });



    });
}

async function insert_file_not_save(url,url_server){
    let result_code = generateUrl();
    url.code  = result_code;
   let r = await url.save((err,respons)=>{
        if(respons){
            console.log(respons);
            return (url_server+'/'+respons.code);
        }
    });
   return r;
}

module.exports = {
    encode,
    openUrl,
    readFile
}
